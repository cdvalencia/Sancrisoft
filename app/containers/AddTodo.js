import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../actions'

let AddTodo = ({ dispatch,editing }) => {  
  let input  
  if(!editing){
     return (
      <div className="addTodo">
        <h2>New item</h2>
        <form onSubmit={e => {
          e.preventDefault()
          if (!input.value.trim()) {
            return
          }        
          dispatch(addTodo(input.value))
          input.value = ''
        }}>
          <input ref={node => {
            input = node
          }} />
          <button type="submit" className="btn">
            Add
          </button>
        </form>
      </div>
    )
  }else{
    return <p></p>
  }
 
}

const mapStateToProps = (state, ownProps) => {      
  return {    
    editing: state.editing
  } 
}

AddTodo = connect(mapStateToProps)(AddTodo)

export default AddTodo
