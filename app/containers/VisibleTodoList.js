import {
	connect
} from 'react-redux'
import {
	toggleTodo, deleteTodo, editTodo, onTodoEditComplete, onEditCancel
} from '../actions'
import TodoList from '../components/TodoList'

var todoCompleto=[];
const getVisibleTodos = (todos, filter) => {	
	switch (filter) {
	case 'SHOW_ALL':
		return todos
	case 'SHOW_COMPLETED':
		return todos.filter(t => t.completed)	
	case 'SHOW_ACTIVE':
		return todos.filter(t => !t.completed)
	case 'DELETE_TODO':		
		return todos.filter(todo => todo.id !== filter.id);
	case 'EDIT_TODO':				
		return todos.filter(todo => todo.id == filter.id);
	case 'EDIT_TODO_COMPLETE':		
		return todos.setState({jasper});
	case 'EDIT_TODO_CANCEL':		
		return todos	
	}

	
}

const countTodos = (todos) => {	
		return todos.filter(todo => todo.completed == true);
}



const mapStateToProps = (state) => {				
	return {
		todos: !state.editing ? getVisibleTodos(state.todos, state.visibilityFilter) : state.todos,		
		edit: state.editing ? state.editing : [],
		selected: countTodos(state.todos),
		// this can be data todo to edit 
		// edit: state.editing	? getVisibleTodos(state.todos, state.visibilityFilter):[],
		loading: state.loading,
		editing: state.editing,
		all: state.visibilityFilter=='SHOW_COMPLETED' || state.visibilityFilter=='SHOW_ACTIVE' ? false:true
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onTodoClick: (id) => {
			dispatch(toggleTodo(id))
		},
		onTodoDelete:(id) => {			
			dispatch(deleteTodo(id))
		},
		onTodoEdit:(id) => {							
			dispatch(editTodo(id))
		},
		onTodoEditComplete:(item) => {			
			dispatch(onTodoEditComplete(item))
		},
		onEditCancel:() => {				
			dispatch(onEditCancel())
		},
		onDeleteSelection:(items) => {	
			items.filter(item => dispatch(deleteTodo(item.id)));				
			
		},
		onSelectAll:(items) => {	
			items.filter(function(item)  {!item.completed ? dispatch(toggleTodo(item.id)):1})
		},
	}
}

const VisibleTodoList = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoList)

export default VisibleTodoList
