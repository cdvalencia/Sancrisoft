const guid=function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


export const addTodo = (text) => {  
  return {
    type: 'ADD_TODO',
    id:guid() ,
    text
  }
}

export const setVisibilityFilter = (filter) => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}

export const toggleTodo = (id) => {
  return {
    type: 'TOGGLE_TODO',
    id
  }
}

export const deleteTodo = (id) => {
  return {
    type: 'DELETE_TODO',
    id
  }
}

export const editTodo = (id) => {
  return {
    type: 'EDIT_TODO',
    id
  }
}

export const onTodoEditComplete = (item) => {
  return {
    type: 'EDIT_TODO_COMPLETE',
    item
  }
}

export const onEditCancel = (item) => {
  return {
    type: 'EDIT_TODO_CANCEL',
    item
  }
}

export const onDeleteSelection = (items) => {
  return {
    type: 'DELETE_SELECTION',
    items
  }
}

export const onSelectAll = (items) => {
  return {
    type: 'SELECT_ALL',
    items
  }
}


