const guid=function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


const todo = (state, action) => {	
	switch (action.type) {
	case 'ADD_TODO':
		return {
			id: action.id,
			text: action.text,
			completed: false
		}
	case 'TOGGLE_TODO':
		if (state.id !== action.id) {
			return state
		}

		return Object.assign({}, state, {
			completed: !state.completed
		})
	case 'DELETE_TODO':		
		return state.filter(todo =>
	        todo.id !== action.id
	    );
		return 
	default:
		return state
	}
}


const todos = (state = [], action) => {	
	switch (action.type) {
	case 'ADD_TODO':
		return [
			...state,
			todo(undefined, Object.assign(action, {
				id: guid()
			}))
		]
	case 'TOGGLE_TODO':
		return state.map(t =>
			todo(t, action)
		)
	case 'GET_TODO_DATA_RECEIVED':
		return action.data
	case 'DELETE_TODO':		
		return state.filter(todo =>
	        todo.id !== action.id
	    );		
	case 'EDIT_TODO':		
		return state
	case 'EDIT_TODO_COMPLETE':			
		var del=state.filter(todo =>
	        todo.id !== action.item.id
	    );			    
	default:
		return state
	}
}

export default todos
