import {
	combineReducers
} from 'redux'
import todos from './todos'
import loading from './loading'
import editing from './editing'
import visibilityFilter from './visibilityFilter'

const todoApp = combineReducers({
	editing,
	loading,
	todos,
	visibilityFilter
})

export default todoApp
