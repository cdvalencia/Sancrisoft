const editing = (state = false, action) => {		
	switch (action.type) {
	case 'GET_TODO_DATA':
		return false
	case 'EDIT_TODO':
		return action.id
	case 'EDIT_TODO_COMPLETE':
		return false
	case 'EDIT_TODO_CANCEL':
		return false
	default:
		return state
	}
}

export default editing
