import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware, compose} from 'redux'
import todoApp from './reducers'
import App from './components/App'
import dataService from './services/data-service'
import style from './styles/main.scss';

let store = createStore(todoApp,{}, compose(applyMiddleware(dataService), window.devToolsExtension
  ? window.devToolsExtension() : f => f))

render(
  <Provider store={store}>
  <App/>
</Provider>, document.getElementById('root'))

store.dispatch({type: 'GET_TODO_DATA'})



