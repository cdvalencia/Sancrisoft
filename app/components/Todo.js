import React, { PropTypes } from 'react'



const Todo = ({ onClick, onDelete, onEdit, onEditComplete, completed, item , text, editing }) => (
  <li 
    style={{
      // Appling fromt view      
      background: completed ? 'rgba(137,72,68,.1)': 'rgba(110,137,111,.1)'
    }}
  >
    <span style={{
      // Appling fromt view
      textDecoration: completed ? 'line-through' : 'none',
      color: completed ? '#894844': '#6E896F',
      // background: completed ? 'rgba(137,72,68,.1)': 'rgba(110,137,111,.1)'
    }} className="content__list-text" onClick={onClick}>{text}</span>
    <div title="Edit" className="content__list-edit icon-pencil" onClick={onEdit}>Edit</div>
    <div title="Cancel" className="content__list-cancel" onClick={onDelete}>X</div>
  </li>
)

Todo.propTypes = {
  item: PropTypes.any,  
  onClick: PropTypes.func.isRequired,
  onEdit: PropTypes.func,  
  onDelete: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  editing: PropTypes.any
}

export default Todo
