import React, { PropTypes } from 'react'

const Link = ({ active, children, onClick, editing }) => {   
  if(!editing){
    if (active) {
      return <span>{children}</span>
    }

    return (
      <a href="#"
         onClick={e => {
           e.preventDefault()
           onClick()
         }}
      >
        {children}
      </a>    
    )
  }else{
    return <p></p>
  }
}

Link.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
  editing: PropTypes.any
}

export default Link
