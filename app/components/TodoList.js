import React, { PropTypes } from 'react'
import Todo from './Todo'
import EditTodo from './EditTodo';


const TodoList = ({ todos, onTodoClick, onTodoDelete, edit, onEditCancel, selected, all,onSelectAll,  onTodoEditComplete, onDeleteSelection, onTodoEdit, loading, editing }) => (  
  <div className="content">

    {editing? (<EditTodo item={edit} onClick={() => onEditCancel()} handleEdit={() => onTodoEditComplete(edit)}/>)
    :   
    (<ul className="content__list">
        <h4>{all ? selected.length + ' of ':''} {todos.length} Items</h4>
        <div className="selectAll" onClick={() => onSelectAll(todos)} >Select All</div>
        {selected.length>0 ? (<div className="deleteAll" onClick={() => onDeleteSelection(selected)} >Delete Selection ({selected.length})</div>): ('')}
        {loading ? 'Loading...': ''}
        
        {todos.map(todo =>
          <Todo
            key={todo.id}
            {...todo}            
            onClick={() => onTodoClick(todo.id)}
            onDelete={() => onTodoDelete(todo.id)}
            onEdit={() => onTodoEdit(todo)}            
            editing={editing}
          />
        )}
      </ul>)

    }    
  </div>
)

TodoList.propTypes = {
  todos: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
  }).isRequired).isRequired,
  onTodoClick: PropTypes.func.isRequired,
  onTodoEdit: PropTypes.func,
  onTodoEditComplete: PropTypes.func,
  onEditCancel: PropTypes.func,
  onTodoDelete: PropTypes.func.isRequired,
  onSelectAll: PropTypes.func,
  onDeleteSelection: PropTypes.func,
	loading: PropTypes.bool,
  editing: PropTypes.any,
  edit: PropTypes.any,
  selected: PropTypes.any,
  all: PropTypes.bool
}

export default TodoList
