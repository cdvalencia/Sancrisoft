import React from 'react'
import FilterLink from '../containers/FilterLink'

const Footer = () => (
  <div className="footer">
    <div className="footer__content">      
      <div className="footer__content-all">
        <FilterLink filter="SHOW_ALL">
          All
        </FilterLink>
      </div>          
      <div className="footer__content-active">
        <FilterLink filter="SHOW_ACTIVE">
          Active
        </FilterLink>
      </div>  
      <div className="footer__content-compelted">      
        <FilterLink filter="SHOW_COMPLETED">
          Completed
        </FilterLink>
      </div>  
    </div>  
  </div>
)

export default Footer
