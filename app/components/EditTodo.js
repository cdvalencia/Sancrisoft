import React, { PropTypes , Component} from 'react'


class EditTodo extends Component {  
  render() {    
    let input;
    const { item, handleEdit, onClick } = this.props;
    return (
      <div className="editTodo">
        <h3>Editing: {item.text}</h3>
        <form onSubmit={e => {
          e.preventDefault()
          if (!input.value.trim()) {
            return
          }     
          item.text=input.value;          
          handleEdit(item);
          input.value = ''
        }}>
          <input defaultValue={item.text} ref={node => {
            input = node
          }} />
          <button type="submit" className="btn-save">
            Save
          </button>
           <button onClick={onClick} className="btn-cancel">
            Cancel
          </button>
        </form>
      </div>
    );
  }
}

EditTodo.propTypes = {  
  item: PropTypes.any,
  handleEdit: PropTypes.func,
  onClick: PropTypes.func,
}

const styles = {
  completed: {
    color: 'gray',
    textDecoration: 'line-through',
  },
};

export default EditTodo;