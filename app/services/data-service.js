import request from 'superagent'

const dataService = store => next => action => {
	next(action)
	switch (action.type) {
	case 'GET_TODO_DATA':
		request
			.get('/list')
			.end((err, res) => {
				if (err) {
					return next({
						type: 'GET_TODO_DATA_ERROR',
						err
					})
				}
				const result = JSON.parse(res.text);				
				var data = result.map(function(item){
					item.completed=item.completed==0?false:true;
					item.id=item.id;					
					return item;
				});				
				next({
					type: 'GET_TODO_DATA_RECEIVED',
					data
				})
			})
		break
	case 'ADD_TODO':			
		request
			.post('/list')
			.send(action)
			.end((err, res) => {
				// Comment 1						
			})		
		break
	case 'DELETE_TODO':		
		request
			.delete('/list/'+action.id)
			.end((err, res) => {
				// Comment 1
			})
		break
	case 'EDIT_TODO_COMPLETE':				
		request
			.put('/list/'+action.item.id)
			.send(action.item)
			.end((err, res) => {
				// Comment 1
			})
		break
	default:
		break
	}

};

export default dataService


/*
Comment 1
if (err) {
	show error and delete action.id
}else{
	Recall SHOW_ALL replacing action.id, with data from server
}	
*/