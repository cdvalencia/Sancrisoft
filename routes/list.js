var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use( bodyParser.json() );
router.use(bodyParser.urlencoded({
  extended: true
}));


var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'test'
});

connection.connect();


/* GET list listing. */
router.get('/', function(req, res, next) {
	connection.query('SELECT * FROM list', function (error, results, fields) {
		res.json(results);
	});
});

router.post('/', function(req, res, next) {
	var data=req.body;
	connection.query("INSERT INTO list (id,text,completed) VALUES ('" +data.id + "', '" +data.text + "', '" + data.completed+"');", function (err, result) {
		// Here send error 
		if(err) return err		
	});
	res.send([{
		id:0,
		text: "dfsdfsdf",
		completed: false
	}])
});

router.delete('/:id', function(req, res, next) {
	var id=req.params.id;
	console.log(id);
	connection.query("DELETE FROM list WHERE id='"+id+"'", function (err, result) {
		console.log(err)
		// Here send error 
		if(err) return err		
		res.send([])
	});
});

router.put('/:id', function(req, res, next) {
	var id=req.params.id;
	var data=req.body;		
	connection.query( "UPDATE list SET text='"+data.text+"' WHERE id='"+id+"'", function (err, result) {
		console.log(err,result)
		// Here send error 
		if(err) return err		
		res.send([])
	});
});


module.exports = router;
