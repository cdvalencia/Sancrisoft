# SancriSoft Test 
This repository content the necesary information, of a todo-engine.

## Getting started

1 - Clone the repository

`git clone https://gitlab.com/cdvalencia/Sancrisoft.git` & `cd Sancrisoft`

2 - install depenedencies

`npm install`

 3 - configure the Database - Mysql, on directory: `/routes/list.js`
```
var connection = mysql.createConnection({
	  host     : 'localhost',
	  user     : 'root',
	  password : 'password',
	  database : 'test'
});
```
4 - execute the script SQL `/DB/Script.sql` or create a table using the name `list`

5 - execute the app

`npm start`

6 - Now your app is ready on  `http://localhost:3000/`
